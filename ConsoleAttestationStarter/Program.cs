﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.IO;
using System.IO.Pipes;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;

namespace ConsoleAttestationStarter
{
    [ServiceContract]
    public interface IPipeInterfaceAttestation
    {
        [OperationContract]
        void attestationOk(bool ok);
    }

    /// <summary>
    /// application used to run the attestation
    /// attestation is in C++ so no WCF pipes there
    /// </summary>
    public class Program
    {

        /// <summary>
        /// start the client pipe and start attestation
        /// </summary>
        public void startClientPipe()
        {

            ChannelFactory<IPipeInterfaceAttestation> pipeFactory =
              new ChannelFactory<IPipeInterfaceAttestation>(
                new NetNamedPipeBinding(),
                new EndpointAddress(
                 "net.pipe://localhost/attestation"));

            IPipeInterfaceAttestation pipeProxy = pipeFactory.CreateChannel();

            try
            {
                
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                
                //settings up parameters for the install process
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.RedirectStandardInput = true;
                startInfo.FileName = "cmd.exe";
                startInfo.UseShellExecute = false;
                
                process.StartInfo = startInfo;

                process.Start();

                String path = getCurrentPath();
                String name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".exe";
                path = path.Replace(name, "");

                process.StandardInput.WriteLine("cd \"" + path + "\"");
                process.StandardInput.WriteLine("cls");
                process.StandardInput.WriteLine("TPM_Full_Attestation.exe");
                
                process.StandardInput.WriteLine("exit");

                process.WaitForExit();

                int status = process.ExitCode;

                Thread.Sleep(1000);

                pipeProxy.attestationOk(status == 0);
                

            }
            catch (Exception e)
            {
                Console.WriteLine("EXC:\n" + e.Message);
                Thread.Sleep(5000);
            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.startClientPipe();
        }

        /// <summary>
        /// get current installation path
        /// </summary>
        /// <returns></returns>
        public static String getCurrentPath()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().Location;
        }
    }
}
