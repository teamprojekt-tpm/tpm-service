﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UntrustedConsole
{
    /// example response for untrusted PC
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Beep(40, 1000);
            Console.WriteLine("Sorry, the princess is in another castle");
            Thread.Sleep(5000);
        }

        /// get current installation path
        public static String getCurrentPath()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().Location;
        }
    }
}
