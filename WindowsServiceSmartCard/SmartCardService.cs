﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
using System.IO.Pipes;
using BasicCard;
using BasicCard.Framework;
using BasicCard.GenericCards;
using BasicCard.BasicCards;
using System.Threading;
using Microsoft.Win32;
using System.ServiceModel;
using System.Reflection;

using ConsoleAttestationStarter;
using ConsoleCheckPin;
using ConsoleCheckPinAddPc;
using TrustedConsole;
using UntrustedConsole;

namespace WindowsServiceSmartCard
{

    /// <summary>
    /// Interface for PIN Console
    /// </summary>
    [ServiceContract]
    public interface IPipeInterface
    {
        [OperationContract]
        void setPin(String pin);
    }

    /// <summary>
    /// Interface for Console to add new PC
    /// </summary>
    [ServiceContract]
    public interface IPipeInterfaceAddingPc
    {
        [OperationContract]
        void setPinForAddingPc(String pin);
    }

    /// <summary>
    /// Interface for Attestation Console
    /// </summary>
    [ServiceContract]
    public interface IPipeInterfaceAttestation
    {
        [OperationContract]
        void attestationOk(bool ok);
    }

    /// <summary>
    /// Interface to Console in case PC is trusted
    /// </summary>
    [ServiceContract]
    public interface IPipeInterfaceTrustedPc
    {
        [OperationContract]
        String getSecret();
    }


    public partial class SmartCardService : ServiceBase, IPipeInterface, IPipeInterfaceAddingPc, IPipeInterfaceAttestation, IPipeInterfaceTrustedPc
    {

        String applicationNameSetPin = ConsoleCheckPin.Program.getCurrentPath();
        String applicationNameSetPinForAddingPc = ConsoleCheckPinAddPc.Program.getCurrentPath();
        String tpmFullAttestationConsole = ConsoleAttestationStarter.Program.getCurrentPath();


        private CardCommunicationService cardService;

        CardReaderObserver cardObserver;
        CardEventHandler cardEventHandler;

        private static SCServiceHoster serviceHoster;
        private String randomNumber;

        public static String pin = "";
        public static String pinForAddingPc = "";

        private static String secret;
        private bool trusted;

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };

        public SmartCardService()
        {
            InitializeComponent();

            String source = "EventLogging";
            String logName = "MyNewLog";

            if (!System.Diagnostics.EventLog.SourceExists(source))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    source, logName);
            }
            eventLog1.Source = source;
            eventLog1.Log = logName;
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);


        /// <summary>
        /// Service Method
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            StartCardservice();
        }

        /// <summary>
        /// Handler for Card Events: Insert, Remove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void handleCardEvent(Object sender, CardEvent e)
        {
            eventLog1.WriteEntry(e.EventType + " sender: " + sender.ToString());

            switch (e.EventType)
            {
                case CardEventType.Insert:

                    cardService =
                        (CardCommunicationService)cardObserver
                        .WaitForCardService(
                    new CardCommunicationService(),
                    new TimeSpan(0, 0, 5));

                    BasicCardResponse answer = null;
                    try
                    {
                        answer = cardService.InitTrustedPath();
                        eventLog1.WriteEntry("IntitTrusted Path finished, now Manage");
                        ManageCardEvents(answer);
                    }
                    catch (CardServiceException ex)
                    {
                        eventLog1.WriteEntry("EXCEPTION!!" + ex);
                    }
                    break;

                case CardEventType.Remove:
                    cardService = null;
                    eventLog1.WriteEntry("removed Card");
                    serviceHoster.Close();
                    break;

                case CardEventType.StatusChange:
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// Implemented Method of IPipeInterface
        /// </summary>
        /// <param name="pin"></param>
        public void setPin(String pin)
        {
            SmartCardService.pin = pin;
        }

        /// <summary>
        /// Implemented Method of IPipeInterfaceAddingPc 
        /// </summary>
        /// <param name="pin"></param>
        public void setPinForAddingPc(String pin)
        {
            SmartCardService.pinForAddingPc = pin;
            eventLog1.WriteEntry("setting pinForAddingPC to: " + pin);
        }

        /// <summary>
        /// Implemented Method for IPipeInterfaceAttestation
        /// </summary>
        /// <param name="ok"></param>
        public void attestationOk(bool ok)
        {
            trusted = ok;
        }

        /// <summary>
        /// Implemented Method for IPipeInterfaceTrustedPc
        /// </summary>
        /// <returns></returns>

        public String getSecret()
        {
            return secret;
        }


        /// <summary>
        /// execute commands from card as long as system is not yet trusted, exited or card is removed
        /// </summary>
        /// <param name="response"></param>
        private void ManageCardEvents(BasicCardResponse response)
        {

            BasicCardResponse rsp = response;


            TrustedPathStatusWord nextCommand = new TrustedPathStatusWord();
            AnswerFromCard answer = null;

            //TODO Only for testing... later with real TPM Value
            String tpmCheckValue = "ABCDEFGHIJKCHECKVALUEXXX";

            do
            {
                Thread.Sleep(500); // for testing...

                BasicCardString rspBasicString = rsp.FetchBasicString();
                String rspString = rspBasicString.ToString(BasicCardString.CharsetIdUtf8);
                try
                {
                    answer = Util.GetAnswer(rspString);
                    nextCommand = (TrustedPathStatusWord)answer.insCode;

                }
                catch (Exception c)
                {
                    eventLog1.WriteEntry("EXCEPTION:" + c.Message);
                }

                BasicCardCommand basicCommand = new BasicCardCommand();
                // always the same CLA byte
                basicCommand.CLA = 0x88;

                Command command = Util.GetCommandFromString(rspString);

                switch (nextCommand)
                {
                    // ask user for PIN first ('1234')
                    case TrustedPathStatusWord.swReadPin:
                        eventLog1.WriteEntry("TrustedPathStatusWord.swReadPin:");

                        // should start console to read pin
                        CreatePipeServerAndProcess(applicationNameSetPin);
                        eventLog1.WriteEntry("got pin: " + pin);
                        BasicCardStringN nStringPin = new BasicCardStringN(4, pin, Encoding.UTF8);

                        basicCommand.AppendBasicString(nStringPin);
                        basicCommand.INS = Convert.ToByte(SmartCardFunctions.cmdCheckPin);

                        eventLog1.WriteEntry("calling card command: " + basicCommand.INS);
                        serviceHoster.Close();

                        break;

                    // is PC already known to card?
                    case TrustedPathStatusWord.swCheckPc:
                        eventLog1.WriteEntry("TrustedPathStatusWord.swCheckPc:");
                        String mac = Util.GetMACAddress();

                        BasicCardStringN nStringMac = new BasicCardStringN(25, mac, Encoding.UTF8);

                        basicCommand.AppendBasicString(nStringMac);
                        basicCommand.INS = Convert.ToByte(SmartCardFunctions.cmdCheckPc);
                        break;

                    case TrustedPathStatusWord.swSetRandomNumber:

                        eventLog1.WriteEntry("TrustedPathStatusWord.swSetRandomNumber (PC KNOWN):");

                        randomNumber = answer.response.First();
                        // TODO Save number

                        //String sessionString = "id";

                        //BasicCardStringN nStringSessionId = new BasicCardStringN(25, sessionString, Encoding.UTF8);

                        //basicCommand.AppendBasicString(nStringSessionId);
                        //basicCommand.INS = Convert.ToByte(SmartCardFunctions.cmdCheckTpmValue);


                        // do attestation and stuff
                        CreatePipeServerAndProcess(tpmFullAttestationConsole);

                        tpmCheckValue = tpmCheckValue + ";;" + trusted;

                        BasicCardStringN nStringCheckTpmValue = new BasicCardStringN(25, tpmCheckValue, Encoding.UTF8);

                        basicCommand.AppendBasicString(nStringCheckTpmValue);
                        basicCommand.INS = Convert.ToByte(SmartCardFunctions.cmdCheckTpmValue);

                        serviceHoster.Close();
                        break;

                    case TrustedPathStatusWord.swGetTpmCheckValue:
                        eventLog1.WriteEntry("TrustedPathStatusWord.swGetTpmCheckValue:");

                        BasicCardStringN nStringTpmValue = new BasicCardStringN(25, tpmCheckValue, Encoding.UTF8);

                        basicCommand.AppendBasicString(nStringTpmValue);
                        basicCommand.INS = Convert.ToByte(SmartCardFunctions.cmdSetTpmValue);

                        break;

                    case TrustedPathStatusWord.swSaveKey:
                        eventLog1.WriteEntry("TrustedPathStatusWord.swSaveKey: \n Key from Card: " + answer.response.First());
                        String session = "sessionId";
                        BasicCardStringN nStringSession = new BasicCardStringN(25, session, Encoding.UTF8);

                        basicCommand.AppendBasicString(nStringSession);
                        basicCommand.INS = Convert.ToByte(SmartCardFunctions.cmdGetSecret);
                        break;

                    // PC is trusted, so reveal the secret
                    case TrustedPathStatusWord.swTrustedSystem:
                        // we did it!
                        eventLog1.WriteEntry("TrustedPathStatusWord.swTrustedSystem:");
                        basicCommand = null;
                        secret = answer.response.First();

                        CreatePipeServerAndProcess(TrustedConsole.Program.getCurrentPath());

                        break;

                    // unknown PC, ask for 'Super-PIN' ('4321')
                    case TrustedPathStatusWord.swCheckPinForAddingPc:
                        eventLog1.WriteEntry("TrustedPathStatusWord.swCheckPinForAddingPc:");
                        // should start console to read pin
                        CreatePipeServerAndProcess(applicationNameSetPinForAddingPc);

                        BasicCardStringN nStringPinAddingPc = new BasicCardStringN(4, pinForAddingPc, Encoding.UTF8);

                        basicCommand.AppendBasicString(nStringPinAddingPc);
                        basicCommand.INS = Convert.ToByte(SmartCardFunctions.cmdCheckPinAddPc);

                        serviceHoster.Close();
                        break;

                    // PC cannot be trusted
                    case TrustedPathStatusWord.swNoTrustedSystem:
                        eventLog1.WriteEntry("TrustedPathStatusWord.swNoTrustedSystem:");
                        eventLog1.WriteEntry("Answer: " + answer.response.First());

                        CreatePipeServerAndProcess(UntrustedConsole.Program.getCurrentPath());

                        break;

                    default:
                        // set to init, just to be sure
                        eventLog1.WriteEntry("TrustedPathStatusWord.DEFAULT");
                        return;
                }

                if (basicCommand != null)
                {
                    eventLog1.WriteEntry("SEND: ");
                    try
                    {
                        rsp = cardService.executeCardCommand(basicCommand);
                        rsp.CheckSW1SW2();
                    }
                    catch (CardServiceException e)
                    {
                        eventLog1.WriteEntry("wrong pin!\n" + e.Message);

                        CreatePipeServerAndProcess(UntrustedConsole.Program.getCurrentPath());
                        throw e;
                    }
                }

            } while (nextCommand != TrustedPathStatusWord.swTrustedSystem);

        }

        /// <summary>
        /// Init CardReaderObserver
        /// </summary>
        private void StartCardservice()
        {
            cardEventHandler = new CardEventHandler(handleCardEvent);
            cardObserver = CardReaderObserver.Instance;
            cardObserver.AddCardEventHandler(cardEventHandler);
            cardObserver.Start();
        }


        /// <summary>
        /// Start application as current user and create WCF Pipe
        /// </summary>
        public void CreatePipeServerAndProcess(String applicationName)
        {
            try
            {
                Thread thread = new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;

                    try
                    {
                        Uri[] uri = { new Uri("net.pipe://localhost") };
                        serviceHoster = new SCServiceHoster(applicationName, eventLog1, uri);
                        serviceHoster.Open();
                    }
                    catch (Exception e)
                    {
                        eventLog1.WriteEntry("Exception: " + e.Message);
                        throw e;
                    }
                });
                thread.Start();
                thread.Join();

            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Exception: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// cleanup because service is stopping
        /// </summary>
        protected override void OnStop()
        {
            cardService.Dispose();
            // Update the service state to Stop Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOP_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Update the service state to Stopped.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnContinue()
        {
            base.OnContinue();
        }

        protected override void OnPause()
        {
            base.OnPause();
        }

        protected override void OnShutdown()
        {

            base.OnShutdown();
        }

        public bool isConnected { get; set; }

    }
}
