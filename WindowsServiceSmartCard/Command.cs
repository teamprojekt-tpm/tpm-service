﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceSmartCard
{
    
    /// <summary>
    /// representation of a command, so a csv separated string vom the card could be used 
    /// </summary>
    class Command
    {
        public Command(String sessionKey, String insCode, String[] parameters)
        {
            this.sessionKey = sessionKey;
            this.insCode = Convert.ToByte(insCode);
            this.parameters = parameters;
        }

        public String sessionKey { get; set; }
        public String[] parameters { get; set; }
        public byte insCode { get; set; }

    }
}
