﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;


namespace WindowsServiceSmartCard
{
    class Util
    { 

        
        /// <summary>
        /// convert csv separated string into Command object
        /// </summary>
        /// <param name="cardString"></param>
        /// <returns></returns>
        public static Command GetCommandFromString(String cardString)
        {
            char[] splitChar = { ';' };
            String[] splittedStringArray = cardString.Split(splitChar);
            if (splittedStringArray.Length < 2)
            {
                // something's wrong in here
                return null;
            }

            String[] sub = SubArray(splittedStringArray, 2, splittedStringArray.Length - 1);
            Command command = new Command(splittedStringArray[0], splittedStringArray[1], sub);
            return command;
        }

        
        /// <summary>
        /// split CardAnswer String into components
        /// </summary>
        /// <param name="responseString"></param>
        /// <returns></returns>       
        public static AnswerFromCard GetAnswer(String responseString)
        {

            String[] splitString = { "##**##" };

            String[] splittedStringArray = responseString.Split(splitString, StringSplitOptions.None);

            if (splittedStringArray.Length < 2)
            {
                // something's wrong in here
                return null;
            }

            //If there is more than one response data in the responseString -> split it again
            String[] responseDataSplitter = { "][][" };
            String[] splittedResponseDataStringArray = splittedStringArray[1].Split(splitString, StringSplitOptions.None);
            
            return new AnswerFromCard(splittedStringArray[0], splittedResponseDataStringArray);;
        }

        
        /// <summary>
        /// get SubArray from existing one
        /// </summary>
        /// <param name="data"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static String[] SubArray(String[] data, int from, int to)
        {
            String[] result = new String[to];
            Array.Copy(data, from, result, 0, to);
            return result;
        }

        
        /// <summary>
        /// get MAC address of current PC
        /// </summary>
        /// <returns></returns>
        public static string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                // only return MAC Address from first card
                if (sMacAddress == String.Empty)  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            } return sMacAddress;
        }
    }
}
