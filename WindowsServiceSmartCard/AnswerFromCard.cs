﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceSmartCard
{
    /// <summary>
    /// representation of a card answer
    /// </summary>
    class AnswerFromCard
    {
        public AnswerFromCard(String nextCommand, String[] responseFromCard)
        {
            this.insCode = Convert.ToByte(nextCommand);
            this.response = responseFromCard;
        }

        public String[] response { get; set; }
        public byte insCode { get; set; }
    }
}
