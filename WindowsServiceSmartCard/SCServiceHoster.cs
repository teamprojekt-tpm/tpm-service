﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsServiceSmartCard
{
    class SCServiceHoster : ServiceHost
    {
        private Process runningProcess;

        public int processId;
        public bool processStarted = false;
        private String applicationName;
        EventLog logger;
        String logTag = "SCServiceHoster:: ";


        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="applicationName"></param>
        /// <param name="myLog"></param>
        /// <param name="uri"></param>
        public SCServiceHoster(String applicationName, EventLog myLog, Uri[] uri)
            : base(typeof(SmartCardService), uri)
        {
            try
            {
                this.logger = myLog;
                this.applicationName = applicationName;
            }
            catch (Exception e)
            {
                logger.WriteEntry(logTag + e.GetType() + ": " + e.Message);
            }

        }


        /// <summary>
        /// another constructor
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="baseAddresses"></param>
        public SCServiceHoster(Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            logger.WriteEntry(logTag + "Base Konstruktor");
        }


        /// <summary>
        /// delegate Method, add pipe service endpoints
        /// </summary>
        protected override void OnOpening()
        {
            base.ApplyConfiguration();
            base.OnOpening();

            try
            {
                //take here IPipeInterface 
                this.AddServiceEndpoint(typeof(IPipeInterface),
                      new NetNamedPipeBinding(),
                      "setPin");
                this.AddServiceEndpoint(typeof(IPipeInterfaceAddingPc),
                      new NetNamedPipeBinding(),
                      "setPinForAddingPc");
                this.AddServiceEndpoint(typeof(IPipeInterfaceAttestation),
                      new NetNamedPipeBinding(),
                      "attestation");
                this.AddServiceEndpoint(typeof(IPipeInterfaceTrustedPc),
                      new NetNamedPipeBinding(),
                      "revealSecret");


            }
            catch (Exception e)
            {
                logger.WriteEntry(logTag + e.GetType() + ": " + e.Message);
            }
        }


        /// <summary>
        /// launch the requested application
        /// </summary>
        protected override void OnOpened()
        {
            base.OnOpened();

            // launch the application
            ApplicationLoader.PROCESS_INFORMATION procInfo;
            ApplicationLoader.StartProcessAndBypassUAC(applicationName, out procInfo);
            processId = (int)procInfo.dwProcessId;
            runningProcess = Process.GetProcessById(processId);

            processStarted = true;

            runningProcess.WaitForExit();

            runningProcess.Close();

        }


        /// <summary>
        /// cleanup
        /// </summary>
        /// <param name="timeout"></param>        
        protected override void OnClose(TimeSpan timeout)
        {
            base.OnClose(timeout);
            processStarted = false;
            logger.WriteEntry(logTag + "Process killed");
        }

    }
}
