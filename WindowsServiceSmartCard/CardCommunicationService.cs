﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasicCard;
using BasicCard.BasicCards;
using BasicCard.GenericCards;

namespace WindowsServiceSmartCard
{

    public enum TrustedPathStatusWord
    {
        swReadPin = 81,//129,//0x6481,
        swCheckPc = 82,//130,//0x6482,
        swSetRandomNumber = 83,//131,//0x6483,
        swGetTpmCheckValue = 84,//132,//0x6484,
        swSaveKey = 85,//133, // 85
        swTrustedSystem = 86,//134,//0x6486,
        swCheckPinForAddingPc = 87,
        swNoTrustedSystem = 88,
    }

    public enum SmartCardFunctions
    {
        cmdInit = 0x02,
        cmdCheckPin = 0x04,
        cmdCheckPc = 0x06,
        cmdCheckTpmValue = 0x08,
        cmdSetTpmValue = 0x10,
        cmdCheckPinAddPc = 0x12,
        cmdGetSecret = 0x14,
    }

    class CardCommunicationService : AbstractBasicCardService
    {

        public CardCommunicationService()
			// specify Application ID of card we like to use
            : base("BasicCard Trusted Path Application")
		{
			doesNeedExclusiveConnection = false; // we like to share the card
			// set the encryption key see Calckeys.bas
			byte[] key={0xD1, 0x47, 0x52, 0xF8,
					   0xE2, 0xD8, 0xFE, 0xDA };
			SetKey(1, key);
		}

        /// <summary>
        /// Activate encryption.
        /// </summary>
        public void StartEncryption()
        {
            // If we are using the card none exclusive,
            // others may access the same card at the same time.
            // This will not work if encryption is active. So
            // in this case lock the card first for exclusive use.
            if (!IsExclusive)
            {
                Lock();
            }
            // Activate encryption by use of autoEncryption function.
            // Depending on BasicCard type either key 1 (single application
            // BasicCard) or key component "\Key1" (MultiApplication BasicCard)
            // is used within card. To use autoEncryption the specified key
            // (1) must be known by the base class AbstractBasicCardService.
            // For this it must have been set by use of setKey function (see
            // CalcService constructor).
            AutoEncryption(1, "\\Key1");
        }

        /// <summary>
        /// Deactivate encryption.
        /// </summary>
        public void EndEncryption()
        {
            // End active encryption
            CmdEndEncryption();
            // Release exclusive use
            if (!IsExclusive)
            {
                Unlock();
            }
        }

        /// <summary>
        /// Init card 
        /// </summary>
        /// <returns></returns>
        public BasicCardResponse InitTrustedPath()
        {

            ResetCard();

            BasicCardCommand cmd = new BasicCardCommand(0x88, (byte)SmartCardFunctions.cmdInit);
            BasicCardResponse rsp = executeCardCommand(cmd);

            return rsp;
        }

        /// <summary>
        /// execute requested command on card
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public BasicCardResponse executeCardCommand(BasicCardCommand command)
        {
           ResetCard();
           return DoCommandAndResponse(command);
        }

    }
}
