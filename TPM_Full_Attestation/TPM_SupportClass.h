#pragma once
#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include "TpmAtt.h"
#include <algorithm>  //for std::generate_n
#include "InlineFn.h"
#include "wbcl.h"
#include "tbs.h"
#include "strsafe.h"


#define EV_PREBOOT_CERT (0x00000000)
#define EV_POST_CODE (0x00000001)
#define EV_UNUSED (0x00000002)
#define EV_NO_ACTION (0x00000003)
#define EV_SEPARATOR (0x00000004)
#define EV_ACTION (0x00000005)
#define EV_EVENT_TAG (0x00000006)
#define EV_S_CRTM_CONTENTS (0x00000007)
#define EV_S_CRTM_VERSION (0x00000008)
#define EV_CPU_MICROCODE (0x00000009)
#define EV_PLATFORM_CONFIG_FLAGS (0x0000000A)
#define EV_TABLE_OF_DEVICES (0x0000000B)
#define EV_COMPACT_HASH (0x0000000C)
#define EV_IPL (0x0000000D)
#define EV_IPL_PARTITION_DATA (0x0000000E)
#define EV_NONHOST_CODE (0x0000000F)
#define EV_NONHOST_CONFIG (0x00000010)
#define EV_NONHOST_INFO (0x00000011)
#define EV_EFI_EVENT_BASE (0x80000000)
#define EV_EFI_VARIABLE_DRIVER_CONFIG (0x80000001)
#define EV_EFI_VARIABLE_BOOT (0x80000002)
#define EV_EFI_BOOT_SERVICES_APPLICATION (0x80000003)
#define EV_EFI_BOOT_SERVICES_DRIVER (0x80000004)
#define EV_EFI_RUNTIME_SERVICES_DRIVER (0x80000005)
#define EV_EFI_GPT_EVENT (0x80000006)
#define EV_EFI_ACTION (0x80000007)
#define EV_EFI_PLATFORM_FIRMWARE_BLOB (0x80000008)
#define EV_EFI_HANDOFF_TABLES (0x80000009)


typedef struct {
    UINT32 Id;
    WCHAR* Name;
} EVENT_TYPE_DATA;

class TPM_SupportClass
{
public:
	TPM_SupportClass(void);
	~TPM_SupportClass(void);

	HRESULT writeFile(_In_ PCWSTR lpFileName, _In_reads_opt_(cbData) PBYTE pbData, UINT32 cbData);
	HRESULT displayKey(_In_ PCWSTR lpKeyName, _In_reads_(cbKey) PBYTE pbKey, DWORD cbKey, UINT32 level);
	HRESULT displayPlatformAttestation(_In_reads_(cbAttestation) PBYTE pbAttestation, DWORD cbAttestation, UINT32 level);
	HRESULT displayLog(_In_reads_opt_(cbWBCL) PBYTE pbWBCL, UINT32 cbWBCL, UINT32 level);
	HRESULT displaySIPA(_In_reads_opt_(cbWBCL) PBYTE pbWBCL, UINT32 cbWBCL, UINT32 level);
	HRESULT readFile(_In_ PCWSTR lpFileName, _In_reads_opt_(cbData) PBYTE pbData, UINT32 cbData, __out PUINT32);
	HRESULT readFile(_In_ PCWSTR lpFileName, UINT32 offset, _In_reads_(cbData) PBYTE pbData, UINT32 cbData);

	void callResult(_In_ WCHAR* func, HRESULT hr);
	void levelPrefix(UINT32 level);
	std::wstring random_string( size_t length );
	std::string execCommand(char* cmd);

};

