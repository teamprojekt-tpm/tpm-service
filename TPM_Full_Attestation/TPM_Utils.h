#pragma once
#define _CRT_SECURE_NO_DEPRECATE

#include <new>

#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include "TpmAtt.h"
#include "tinyxml.h"
#include <algorithm>  //for std::generate_n
#include "InlineFn.h"
#include "wbcl.h"
#include <string>
#include <map>
#include "TPM_SupportClass.h"

using namespace std;

class TPM_Utils
{
public:
	TPM_SupportClass supporter;

	TPM_Utils(void);
	~TPM_Utils(void);
	bool runAttestation();
private:

	// IdBinding File contains the public key of the AIK, gets exported in createAIK()
	std::wstring idBindingFileGlobal;
	
	// TPM EK public key from getEK()
	std::wstring ekPubFileGlobal;
	
	// Random generated string AIK key, parameter for createAIK()
	std::wstring keyAIKGlobal;
	
	// Secret String which get entrypted with EKPub, in challangeAIK()
	std::wstring secretGlobal;
	
	// Gets created in challangeAIK(), important for activateAIK()
	std::wstring activationBlobFileGlobal;

	// Exported public key file from getPubKey()
	std::wstring publicKeyFileGlobal;

	// Exported public key file from getPubAIK(), same output like getPubKey
	std::wstring publicAikFromGetPubAikGlobal;
	
	// Attestation file from getPlatformAttestation
	std::wstring attestationBlobGlobal;

	// Nonce for getPlatformAttestation()
	std::wstring attestationNonceGlobal;

	// Logfile from getLog()
	std::wstring exportedLogFile;

	// Xml Logfile for the measured boot tool
	string exportedPlatformAttAsXml;

	// Logfile from last booted log
	std::wstring lastBootLogFile;

	// Attestation File for TrustPointValidation, used in createPlatformAttestationFromLog()
	std::wstring trustedPointCurrentAttestation;

	// Attestation File from last boot, used in createPlatformAttestationFromLog()
	std::wstring lastBootAttestation;

	// Boot count parameter for GetArchivedLog()
	std::wstring oSBootCountParam;

	// Resume count parameter for GetArchivedLog()
	std::wstring oSResumeCountParam;

	// Command String for bcdedit
	char* commandIntegrityServices;
	
	// Toogle Output from pcpTool
	bool logging;



	// AIK-Creation
	HRESULT aik_Creation_Routine();
	// Attestation
	HRESULT attestation_Routine();
	// TrustedPointValidation
	HRESULT trustedPointValidation_Routine();
	// run the measuredBootTool 
	HRESULT measuredBootTool_Routine();

	void printTpmVersion();

	/* AIK Creation */
	HRESULT getEK();
	HRESULT createAIK();
	HRESULT getPubAIK();
	HRESULT challengeAIK();
	HRESULT activateAIK();
	HRESULT getPubKey();
	HRESULT registerAIK();

	/* Attestation */
	HRESULT getPlatformAttestation(bool generateXml);
	HRESULT validatePlatformAttestation(std::wstring _attestation, std::wstring _aikPub);

	/* Trusted Point Validation */
	HRESULT getLog();
	HRESULT createPlatformAttestationFromLog(std::wstring _logFile, std::wstring _attestationFile);
	HRESULT getArchivedLog();
	bool runMeasuredBoot();
};

