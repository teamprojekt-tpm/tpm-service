#include "TPM_Utils.h"



TPM_Utils::TPM_Utils(void)
{
	// Support class for additional tpm functionality
	supporter = TPM_SupportClass();
	// Print tpm verion
	printTpmVersion();
	// Set some filenames, nonce, ect. ...
	idBindingFileGlobal = L"idBinding";
	ekPubFileGlobal = L"ekPub";
	keyAIKGlobal = supporter.random_string( 20 ).c_str();
	secretGlobal = L"MYSECRET";
	activationBlobFileGlobal = L"myActivationBlobFile";
	publicKeyFileGlobal = L"pubKeyFile";
	publicAikFromGetPubAikGlobal = L"pubAIK_sameLikePubKey";
	attestationBlobGlobal = L"myAttestationBlobFile";
	attestationNonceGlobal = L"nonceforAttestation";
	exportedLogFile = L"log_exported";
	trustedPointCurrentAttestation = L"currentAttestation";
	lastBootAttestation = L"lastBootAttestation";
	lastBootLogFile = L"lastBootLogFile";
	exportedPlatformAttAsXml = "output.xml";
	// read doc for params, @ = user choose usageAuth , ! = PIN requeired usageAuth
	oSBootCountParam = L"@";
	oSResumeCountParam = L"@";
	
	// To enable integrityservices
	commandIntegrityServices = "bcdedit -set integrityservices enable";
	
	//Logging off
	logging = false;
}


TPM_Utils::~TPM_Utils(void)
{

	std::string sId( idBindingFileGlobal.begin(), idBindingFileGlobal.end() );
	std::string sEkPubFileGlobal(std::string(ekPubFileGlobal.begin(), ekPubFileGlobal.end()));
	std::string sActivationBlobFileGlobal(std::string(activationBlobFileGlobal.begin(), activationBlobFileGlobal.end()));
	std::string sPublicKeyFileGlobal(std::string(publicKeyFileGlobal.begin(), publicKeyFileGlobal.end()));
	std::string sPublicAikFromGetPubAikGlobal(std::string(publicAikFromGetPubAikGlobal.begin(), publicAikFromGetPubAikGlobal.end()));
	std::string sAttestationBlobGlobal(std::string(attestationBlobGlobal.begin(), attestationBlobGlobal.end()));
	std::string sExportedLogFile(std::string(exportedLogFile.begin(), exportedLogFile.end()));
	std::string sTrustedPointCurrentAttestation(std::string(trustedPointCurrentAttestation.begin(), trustedPointCurrentAttestation.end()));
	std::string sLastBootAttestation(std::string(lastBootAttestation.begin(), lastBootAttestation.end()));
	std::string sExportedPlatformAttAsXml(std::string(exportedPlatformAttAsXml.begin(), exportedPlatformAttAsXml.end()));
	std::string sLastBootLogFile(std::string(lastBootLogFile.begin(), lastBootLogFile.end()));
	
	remove(sId.c_str());
	remove(sEkPubFileGlobal.c_str());
	remove(sActivationBlobFileGlobal.c_str());
	remove(sPublicKeyFileGlobal.c_str());
	remove(sPublicAikFromGetPubAikGlobal.c_str());
	remove(sAttestationBlobGlobal.c_str());
	remove(sExportedLogFile.c_str());
	remove(sTrustedPointCurrentAttestation.c_str());
	remove(sLastBootAttestation.c_str());
	remove(sExportedPlatformAttAsXml.c_str());
	remove(sLastBootLogFile.c_str());

}

bool TPM_Utils::runAttestation()
{
	bool result = true;
	if(!SUCCEEDED(aik_Creation_Routine())) {result = false;}
	if(!SUCCEEDED(attestation_Routine())) {result = false;}
	if(!SUCCEEDED(trustedPointValidation_Routine())) {result = false;}
	if(!measuredBootTool_Routine()) {result = false;}

	if(!result) {
		printf("FAILED\n");
		printf("Maybe this command could help?:\n");
		printf(commandIntegrityServices);
		printf("\n");
	}

	return result;
}


HRESULT TPM_Utils::aik_Creation_Routine()
{
	HRESULT hr = S_OK;
	printf("\n");
	printf("+++++++++++ Start AIK Creation... ++++++++++\n");
	printf("\n");
	printf("Get EK: ");
	(hr = SUCCEEDED(getEK())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Create AIK: ");
	(hr = SUCCEEDED(createAIK())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Get PubAIK: ");
	(hr = SUCCEEDED(getPubAIK())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Challenge: ");
	(hr = SUCCEEDED(challengeAIK())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Activate: ");
	(hr = SUCCEEDED(activateAIK())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Register AIK: ");
	(hr = SUCCEEDED(registerAIK())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("\n");
	printf("------------ AIK Creation finished -----------\n");
	printf("\n");
	return hr;
}


HRESULT TPM_Utils::attestation_Routine()
{
	HRESULT hr = S_OK;
	printf("\n");
	printf("+++++++++++ Start Attestation ... ++++++++++\n");
	printf("\n");
	printf("Get Platform Attestation: ");
	(hr = SUCCEEDED(getPlatformAttestation(false))) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Validate Platform Attestation: ");
	(hr = SUCCEEDED(validatePlatformAttestation(attestationBlobGlobal, publicAikFromGetPubAikGlobal)) ) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("\n");
	printf("----------- Attestation finished -----------\n");
	printf("\n");
	return hr;
}


HRESULT TPM_Utils::trustedPointValidation_Routine()
{
	HRESULT hr = S_OK;
	printf("\n");
	printf("+++++++++++ Start Trusted Point Validation ... ++++++++++\n");
	printf("\n");
	printf("Get PubKey: ");
	(hr = SUCCEEDED(getPubKey())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Get Log: ");
	(hr = SUCCEEDED(getLog())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Create Platform Attestation from Log: ");
	(hr = SUCCEEDED(createPlatformAttestationFromLog(exportedLogFile, trustedPointCurrentAttestation))) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Validate Platform Attestation: ");
	(hr = SUCCEEDED(validatePlatformAttestation(trustedPointCurrentAttestation, publicKeyFileGlobal))) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");	
	printf("Validate Platform Attestation by PCRs: ");	
	(hr = SUCCEEDED(validatePlatformAttestation(trustedPointCurrentAttestation, L""))) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Get Archived Log: ");
	(hr = SUCCEEDED(getArchivedLog())) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Create Platform Attestation from last log:  ");
	(hr = SUCCEEDED(createPlatformAttestationFromLog(lastBootLogFile, lastBootAttestation))) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("Validate Platform Attestation from last boot log: ");	
	(hr = SUCCEEDED(validatePlatformAttestation(lastBootAttestation, publicKeyFileGlobal))) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");
	printf("\n");
	printf("----------- Trusted Point Validation finished -----------");
	printf("\n");

	Sleep(2000);
	return hr;
}

HRESULT TPM_Utils::measuredBootTool_Routine()
{
	bool valid = true;
	printf("\n");
	printf("+++++++++++ Run Measured Boot Tool ... ++++++++++\n");
	printf("\n");
	printf("Check Files: \n");
	(valid = runMeasuredBoot()) ? (cout << "OK! \n") : (cout << "FAILED \n");
	printf("\n");

	Sleep(2000);
	return valid;
}


void TPM_Utils::printTpmVersion() 
{
	UINT32 tpmVersion = 0;
	HRESULT result = TpmAttiGetTpmVersion(&tpmVersion);
	char *a = "";
	switch(tpmVersion) {
	case 1:
		a = "1.2";
		break;
	case 2:
		a = "2.0";
		break;
	default:
		a = "UNKNOWN";
	};
	cout<< "TPM version: " << a << "\n";
}

HRESULT TPM_Utils::getEK()
/*++
Retrieve the EKPub from the TPM through the PCP. The key is provided as a
BCRYPT_RSAKEY_BLOB structure.
--*/
{
	HRESULT hr = S_OK;
    PCWSTR fileName = NULL;
    NCRYPT_PROV_HANDLE hProv = NULL;
    BYTE pbEkPub[1024] = {0};
    DWORD cbEkPub = 0;

	fileName = ekPubFileGlobal.c_str();

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenStorageProvider(
                            &hProv,
                            MS_PLATFORM_CRYPTO_PROVIDER,
                            0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptGetProperty(hProv,
                            NCRYPT_PCP_EKPUB_PROPERTY,
                            pbEkPub,
                            sizeof(pbEkPub),
                            &cbEkPub,
                            0))))
    {
        goto Cleanup;
    }

    if((fileName != NULL) &&
		(FAILED(hr = supporter.writeFile(fileName, pbEkPub, cbEkPub))))
    {
        goto Cleanup;
    }
	if(logging) {
		if(FAILED(hr = supporter.displayKey(L"EndorsementKey", pbEkPub, cbEkPub, 0)))
		{
			goto Cleanup;
		}
	}
Cleanup:
    if(hProv != NULL)
    {
        NCryptFreeObject(hProv);
        hProv = NULL;
    }
    supporter.callResult(L"getEK()", hr);
    return hr;
}


HRESULT TPM_Utils::createAIK()

/*++
This function will create an AIK. The AIK is completely usable after creation.
If strong remote trust has to be established in that key for platform or key
attestation for example, the AIK handshake has to be used.

This is the second step in the AIK handshake: Step one is a nonce that is
randomly generated by the validator of the AIK. In this step the client creates
the key and Identity Binding, that is the proof of posession.
--*/
{
    HRESULT hr = S_OK;
    NCRYPT_PROV_HANDLE hProv = NULL;
    NCRYPT_KEY_HANDLE hKey = NULL;
    DWORD dwKeyUsage = NCRYPT_PCP_IDENTITY_KEY;
    PCWSTR keyName = NULL;
    PCWSTR idBindingFile = NULL;
    PCWSTR nonce = NULL;
    PCWSTR usageAuth = NULL;
    BYTE pbIdBinding[1024] = {0};
    DWORD cbIdBinding = 0;
    BYTE pbAikPub[1024] = {0};
    DWORD cbAikPub = 0;
    BYTE nonceDigest[20] = {0};
    UINT32 result = 0;
    BOOLEAN tUIRequested = false;
    LPCWSTR optionalPIN = L"This AIK requires usage consent and an optional PIN.";
    LPCWSTR mandatoryPIN = L"This AIK has a mandatory a PIN.";
    NCRYPT_UI_POLICY rgbUiPolicy = {1, 0, L"PCPTool", NULL, NULL};


    // Mandatory parameter: Key Name
	// NEW - createRandom String
	keyName = keyAIKGlobal.c_str();


    // Optional parameter: File to store IdBinding
	idBindingFile = idBindingFileGlobal.c_str();

    // Optional parameter: nonce
    /*
	if((argc > 4) && (argv[4] != NULL) && (wcslen(argv[4]) != 0))
    {
        nonce = argv[4];
        if(FAILED(hr = TpmAttiShaHash(
                            BCRYPT_SHA1_ALGORITHM,
                            NULL,
                            0,
                            (PBYTE)nonce,
                            (UINT32)(wcslen(nonce) * sizeof(WCHAR)),
                            nonceDigest,
                            sizeof(nonceDigest),
                            &result)))
        {
            goto Cleanup;
        }
    }
	*/

	/*
    // Optional parameter: usageAuth
    if((argc > 5) && (argv[5] != NULL) && (wcslen(argv[5]) != 0))
    {
        usageAuth = argv[5];
        if(!wcscmp(usageAuth, L"@"))
        {
            // Caller requested UI
            usageAuth = NULL;
            tUIRequested = TRUE;
            rgbUiPolicy.pszFriendlyName = keyName;
            rgbUiPolicy.dwFlags = NCRYPT_UI_PROTECT_KEY_FLAG;
            rgbUiPolicy.pszDescription = optionalPIN;
        }
        else if(!wcscmp(usageAuth, L"!"))
        {
            // Caller requested UI
            usageAuth = NULL;
            tUIRequested = TRUE;
            rgbUiPolicy.pszFriendlyName = keyName;
            rgbUiPolicy.dwFlags = NCRYPT_UI_FORCE_HIGH_PROTECTION_FLAG;
            rgbUiPolicy.pszDescription = mandatoryPIN;
        }
    }
	*/

    // Create the AIK
    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenStorageProvider(
                                &hProv,
                                MS_PLATFORM_CRYPTO_PROVIDER,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptCreatePersistedKey(
                                hProv,
                                &hKey,
                                BCRYPT_RSA_ALGORITHM,
                                keyName,
                                0,
                                NCRYPT_OVERWRITE_KEY_FLAG))))
    {
        goto Cleanup;
    }

    if(tUIRequested == FALSE)
    {
        if((usageAuth != NULL) && (wcslen(usageAuth) != 0))
        {
            if(FAILED(hr = HRESULT_FROM_WIN32(NCryptSetProperty(
                                        hKey,
                                        NCRYPT_PIN_PROPERTY,
                                        (PBYTE)usageAuth,
                                        (DWORD)((wcslen(usageAuth) + 1) * sizeof(WCHAR)),
                                        0))))
            {
                goto Cleanup;
            }
        }
    }
    else
    {
        if(FAILED(hr = HRESULT_FROM_WIN32(NCryptSetProperty(
                                    hKey,
                                    NCRYPT_UI_POLICY_PROPERTY,
                                    (PBYTE)&rgbUiPolicy,
                                    sizeof(NCRYPT_UI_POLICY),
                                    0))))
        {
            goto Cleanup;
        }
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptSetProperty(
                                hKey,
                                NCRYPT_PCP_KEY_USAGE_POLICY_PROPERTY,
                                (PBYTE)&dwKeyUsage,
                                sizeof(dwKeyUsage),
                                0))))
    {
        goto Cleanup;
    }

    if(nonce != NULL)
    {
        if(FAILED(hr = HRESULT_FROM_WIN32(NCryptSetProperty(hKey,
                                      NCRYPT_PCP_TPM12_IDBINDING_PROPERTY,
                                      nonceDigest,
                                      sizeof(nonceDigest),
                                      0))))
        {
            goto Cleanup;
        }
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptFinalizeKey(hKey, 0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptExportKey(
                                hKey,
                                NULL,
                                BCRYPT_RSAPUBLIC_BLOB,
                                NULL,
                                pbAikPub,
                                sizeof(pbAikPub),
                                &cbAikPub,
                                0))))
    {
        goto Cleanup;
    }


    // Store the IdBinding
    if(idBindingFile != NULL)
    {
        if(FAILED(hr = HRESULT_FROM_WIN32(NCryptGetProperty(hKey,
                                      NCRYPT_PCP_TPM12_IDBINDING_PROPERTY,
                                      pbIdBinding,
                                      sizeof(pbIdBinding),
                                      &cbIdBinding,
                                      0))))
        {
            goto Cleanup;
        }
		
        if(FAILED(hr = supporter.writeFile(
                                idBindingFile,
                                pbIdBinding,
                                cbIdBinding)))
        {
            goto Cleanup;
        }

    }



    // Output results
	if(logging) {
		wprintf(L"<AIK>\n");
		if(FAILED(hr = supporter.displayKey(keyName, pbAikPub, cbAikPub, 1)))
		{
			goto Cleanup;
		}
		supporter.levelPrefix(1);
		wprintf(L"<IdentityBinding size=\"%u\">", cbIdBinding);
		for(UINT32 n = 0; n < cbIdBinding; n++)
		{
			wprintf(L"%02x", pbIdBinding[n]);
		}
		wprintf(L"</IdentityBinding>\n");
		wprintf(L"</AIK>\n");
	}
Cleanup:
    if(hKey != NULL)
    {
        NCryptFreeObject(hKey);
        hKey = NULL;
    }
    if(hProv != NULL)
    {
        NCryptFreeObject(hProv);
        hProv = NULL;
    }
    supporter.callResult(L"createAIK()", hr);
    return hr;
}


HRESULT TPM_Utils::getPubAIK()
/*++
Export the public portion from an AIK as
BCRYPT_RSAKEY_BLOB structure.
--*/
{
    HRESULT hr = S_OK;
    PCWSTR idBindingFile = NULL;
    PBYTE pbIdBinding = NULL;
    UINT32 cbIdBinding = 0;
    BCRYPT_ALG_HANDLE hProv = NULL;
    BCRYPT_KEY_HANDLE hKey = NULL;
    PCWSTR keyFile = NULL;
    BYTE pbPubKey[1024] = {0};
    DWORD cbPubKey = 0;


    // Mandatory Parameter: IdBinding
	idBindingFile = idBindingFileGlobal.c_str();
	
	if(FAILED(hr = supporter.readFile(
						idBindingFile,
						NULL,
						0,
						&cbIdBinding)))
	{
		goto Cleanup;
	}
	if(FAILED(hr = AllocateAndZero((PVOID*)&pbIdBinding, cbIdBinding)))
	{
		goto Cleanup;
	}
	if(FAILED(hr = supporter.readFile(idBindingFile,
								   pbIdBinding,
								   cbIdBinding,
								   &cbIdBinding)))
	{
		goto Cleanup;
	}
		

    // Optional parameter: Export file
	
	//TODO - In doku steht: It produces the same output as getPubKey. Typically run on server to generate certificate with the AIKPUB

	keyFile = publicAikFromGetPubAikGlobal.c_str();

    // Open key
    if(FAILED(hr = BCryptOpenAlgorithmProvider(
                                &hProv,
                                BCRYPT_RSA_ALGORITHM,
                                NULL,
                                0)))
    {
        goto Cleanup;
    }

    if(FAILED(hr = TpmAttPubKeyFromIdBinding(
                                pbIdBinding,
                                cbIdBinding,
                                hProv,
                                &hKey)))
    {
        goto Cleanup;
    }

    // Export public key
    if(FAILED(hr = BCryptExportKey(
                                hKey,
                                NULL,
                                BCRYPT_RSAPUBLIC_BLOB,
                                pbPubKey,
                                sizeof(pbPubKey),
                                &cbPubKey,
                                0)))
    {
        goto Cleanup;
    }

    // Export key
    
	//TODO - In doku steht: It produces the same output as getPubKey. Typically run on server to generate certificate with the AIKPUB

	if(keyFile != NULL)
    {
        if(FAILED(hr = supporter.writeFile(keyFile, pbPubKey, cbPubKey)))
        {
            goto Cleanup;
        }
    }
	

    // Output results
	if(logging) {
		if(FAILED(hr = supporter.displayKey(L"AIK", pbPubKey, cbPubKey, 0)))
		{
			goto Cleanup;
		}
	}
Cleanup:
    if(hKey != NULL)
    {
        BCryptDestroyKey(hKey);
        hKey = NULL;
    }
    if(hProv != NULL)
    {
        BCryptCloseAlgorithmProvider(hProv, 0);
        hProv = NULL;
    }
    ZeroAndFree((PVOID*)&pbIdBinding, cbIdBinding);
    supporter.callResult(L"getPubAIK()", hr);
    return hr;

}

HRESULT TPM_Utils::challengeAIK()
/*++
This function is the third step in what is known as the AIK handshake and
executed on the server. The server will have already looked at the EKCert and
extracted the EKPub from it. This step will generate the activation blob, which
is the challenge to the client. The secret may be a nonce or a symmetric key
that encrypts a certificate for the AIK for example.
--*/
{
    HRESULT hr = S_OK;
    PCWSTR idBindingFile = NULL;
    PCWSTR ekPubFile = NULL;
    PCWSTR activationSecret = NULL;
    PCWSTR activationBlobFile = NULL;
    PCWSTR nonce = NULL;
    BCRYPT_ALG_HANDLE hAlg = NULL;
    BCRYPT_KEY_HANDLE hEK = NULL;
    BCRYPT_KEY_HANDLE hAik = NULL;
    PBYTE pbIdBinding = NULL;
    UINT32 cbIdBinding = 0;
    PBYTE pbEkPub = NULL;
    UINT32 cbEkPub = 0;
    PBYTE pbAikPub = NULL;
    UINT32 cbAikPub = 0;
    BYTE nonceDigest[20] = {0};
    PBYTE pbActivationBlob = NULL;
    UINT32 cbActivationBlob = 0;
    UINT32 result = 0;

    // Mandatory Parameter: IdBinding
	idBindingFile = idBindingFileGlobal.c_str();
	

	if(FAILED(hr = supporter.readFile(
	                    idBindingFile,
	                    NULL,
	                    0,
	                    &cbIdBinding)))
	{
	    goto Cleanup;
	}
	if(FAILED(hr = AllocateAndZero((PVOID*)&pbIdBinding, cbIdBinding)))
	{
	    goto Cleanup;
	}
	if(FAILED(hr = supporter.readFile(idBindingFile,
	                               pbIdBinding,
	                               cbIdBinding,
	                               &cbIdBinding)))
	{
	    goto Cleanup;
	}
	
	
    // Mandatory Parameter: EKPub
	ekPubFile = ekPubFileGlobal.c_str();
    if(FAILED(hr = supporter.readFile(ekPubFile, NULL, 0, &cbEkPub)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbEkPub, cbEkPub)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = supporter.readFile(ekPubFile,
                                   pbEkPub,
                                   cbEkPub,
                                   &cbEkPub)))
    {
        goto Cleanup;
    }


    // Mandatory Parameter: Activation secret
	activationSecret = secretGlobal.c_str();


    // Optional parameter: Activation blob
	activationBlobFile = activationBlobFileGlobal.c_str();
    
/*
    // Optional parameter: Nonce
    if(argc > 6)
    {
        nonce = argv[6];
        if(FAILED(hr = TpmAttiShaHash(
                            BCRYPT_SHA1_ALGORITHM,
                            NULL,
                            0,
                            (PBYTE)nonce,
                            (UINT32)(wcslen(nonce) * sizeof(WCHAR)),
                            nonceDigest,
                            sizeof(nonceDigest),
                            &result)))
        {
            goto Cleanup;
        }
    }
	*/
    // Load the keys
    if(FAILED(hr = HRESULT_FROM_NT(BCryptOpenAlgorithmProvider(
                                &hAlg,
                                BCRYPT_RSA_ALGORITHM,
                                MS_PRIMITIVE_PROVIDER,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_NT(BCryptImportKeyPair(
                                hAlg,
                                NULL,
                                BCRYPT_RSAPUBLIC_BLOB,
                                &hEK,
                                pbEkPub,
                                cbEkPub,
                                0))))
    {
        goto Cleanup;
    }

    // Get a handle to the AIK and export it
    if(FAILED(hr = TpmAttPubKeyFromIdBinding(
                        pbIdBinding,
                        cbIdBinding,
                        hAlg,
                        &hAik)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = HRESULT_FROM_NT(BCryptExportKey(
                                hAik,
                                NULL,
                                BCRYPT_RSAPUBLIC_BLOB,
                                NULL,
                                0,
                                (PULONG)&cbAikPub,
                                0))))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbAikPub, cbAikPub)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = HRESULT_FROM_NT(BCryptExportKey(
                                hAik,
                                NULL,
                                BCRYPT_RSAPUBLIC_BLOB,
                                pbAikPub,
                                cbAikPub,
                                (PULONG)&cbAikPub,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = TpmAttGenerateActivation(
                        hEK,
                        pbIdBinding,
                        cbIdBinding,
                        (nonce) ? nonceDigest : NULL,
                        (nonce) ? sizeof(nonceDigest) : 0,
                        (PBYTE)activationSecret,
                        (UINT16)((wcslen(activationSecret) + 1) *
                            sizeof(WCHAR)),
                        NULL,
                        0,
                        &cbActivationBlob)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbActivationBlob, cbActivationBlob)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = TpmAttGenerateActivation(
                        hEK,
                        pbIdBinding,
                        cbIdBinding,
                        (nonce) ? nonceDigest : NULL,
                        (nonce) ? sizeof(nonceDigest) : 0,
                        (PBYTE)activationSecret,
                        (UINT16)((wcslen(activationSecret) + 1) *
                            sizeof(WCHAR)),
                        pbActivationBlob,
                        cbActivationBlob,
                        &cbActivationBlob)))
    {
        goto Cleanup;
    }

    // Store the activation if required
    if(idBindingFile != NULL)
    {
        if(FAILED(hr = supporter.writeFile(
                                activationBlobFile,
                                pbActivationBlob,
                                cbActivationBlob)))
        {
            goto Cleanup;
        }
    }

    // Output results
	if(logging) {
		wprintf(L"<Activation>\n");
		if(FAILED(hr = supporter.displayKey(L"AIK", pbAikPub, cbAikPub, 1)))
		{
			goto Cleanup;
		}
		supporter.levelPrefix(1);
		wprintf(L"<ActivationBlob size=\"%u\">\n", cbActivationBlob);
		supporter.levelPrefix(2);
		for(UINT32 n = 0; n < cbActivationBlob; n++)
		{
			wprintf(L"%02x", pbActivationBlob[n]);
		}
		wprintf(L"\n");
		supporter.levelPrefix(1);
		wprintf(L"</ActivationBlob>\n");
		wprintf(L"</Activation>\n");
	}
Cleanup:
    if(hEK != NULL)
    {
        BCryptDestroyKey(hEK);
        hEK = NULL;
    }
    if(hAik != NULL)
    {
        BCryptDestroyKey(hAik);
        hAik = NULL;
    }
    if(hAlg != NULL)
    {
        BCryptCloseAlgorithmProvider(hAlg, 0);
        hAlg = NULL;
    }
    ZeroAndFree((PVOID*)&pbIdBinding, cbIdBinding);
    ZeroAndFree((PVOID*)&pbEkPub, cbEkPub);
    ZeroAndFree((PVOID*)&pbAikPub, cbAikPub);
    ZeroAndFree((PVOID*)&pbActivationBlob, cbActivationBlob);
    supporter.callResult(L"challengeAIK()", hr);
    return hr;
}

HRESULT TPM_Utils::activateAIK() 
/*++
This function is the last step in what is known as the AIK handshake. The client
will load the AIK into the TPM and perform the activation to retieve the secret
challenge. If the specified EK and AIK reside in the same TPM, it will release
the secret. The secret may be a nonce or a symmetric key that protects other
data that may be release if the handshake was successful.
--*/
{
    HRESULT hr = S_OK;
    NCRYPT_PROV_HANDLE hProv = NULL;
    NCRYPT_KEY_HANDLE hKey = NULL;
    PCWSTR keyName = NULL;
    PCWSTR activationFile = NULL;
    PBYTE pbActivationBlob = NULL;
    UINT32 cbActivationBlob = 0;
    BYTE pbSecret[256] = {0};
    DWORD cbSecret = 0;


    // Mandatory Parameter: AIK name
	keyName = keyAIKGlobal.c_str();

    // Mandatory Parameter: Activation blob
	activationFile = activationBlobFileGlobal.c_str();
	if(FAILED(hr = supporter.readFile(activationFile,
								   NULL,
								   0,
								   &cbActivationBlob)))
	{
		goto Cleanup;
	}
	if(FAILED(hr = AllocateAndZero((PVOID*)&pbActivationBlob, cbActivationBlob)))
	{
		goto Cleanup;
	}
	if(FAILED(hr = supporter.readFile(activationFile,
								   pbActivationBlob,
								   cbActivationBlob,
								   &cbActivationBlob)))
	{
		goto Cleanup;
	}

    // Open AIK
    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenStorageProvider(
                                &hProv,
                                MS_PLATFORM_CRYPTO_PROVIDER,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenKey(
                                hProv,
                                &hKey,
                                keyName,
                                0,
                                0))))
    {
        goto Cleanup;
    }

    // Perform the activation
    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptSetProperty(
                                hKey,
                                NCRYPT_PCP_TPM12_IDACTIVATION_PROPERTY,
                                pbActivationBlob,
                                cbActivationBlob,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptGetProperty(
                                hKey,
                                NCRYPT_PCP_TPM12_IDACTIVATION_PROPERTY,
                                pbSecret,
                                sizeof(pbSecret),
                                &cbSecret,
                                0))))
    {
        goto Cleanup;
    }

    // Output results
	if(logging) {
		wprintf(L"<Activation>\n");
		supporter.levelPrefix(1);
		wprintf(L"<Secret size=\"%u\">%s</Secret>\n", cbSecret, (PWCHAR)pbSecret);
		wprintf(L"</Activation>\n");
	}
Cleanup:
    if(hKey != NULL)
    {
        NCryptFreeObject(hKey);
        hKey = NULL;
    }
    if(hProv != NULL)
    {
        NCryptFreeObject(hProv);
        hProv = NULL;
    }
    ZeroAndFree((PVOID*)&pbActivationBlob, cbActivationBlob);
    supporter.callResult(L"activateAIK()", hr);
    return hr;
}

HRESULT TPM_Utils::getPubKey()
/*++
Export the public portion from a user key in the PCP storage as
BCRYPT_RSAKEY_BLOB structure.
--*/
{
    HRESULT hr = S_OK;
    NCRYPT_PROV_HANDLE hProv = NULL;
    NCRYPT_KEY_HANDLE hKey = NULL;
    PCWSTR keyName = NULL;
    PCWSTR keyFile = NULL;
    BYTE pbPubKey[1024] = {0};
    DWORD cbPubKey = 0;


    // Mandatory parameter: Key Name
	keyName = keyAIKGlobal.c_str();


    // Optional parameter: Export file
	keyFile = publicKeyFileGlobal.c_str();


    // Open key
    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenStorageProvider(
                                &hProv,
                                MS_PLATFORM_CRYPTO_PROVIDER,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenKey(
                                hProv,
                                &hKey,
                                keyName,
                                0,
                                0))))
    {
        goto Cleanup;
    }

    // Export public key
    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptExportKey(
                                hKey,
                                NULL,
                                BCRYPT_RSAPUBLIC_BLOB,
                                NULL,
                                pbPubKey,
                                sizeof(pbPubKey),
                                &cbPubKey,
                                0))))
    {
        goto Cleanup;
    }

    // Export key
    if(keyFile != NULL)
    {
        if(FAILED(hr = supporter.writeFile(keyFile, pbPubKey, cbPubKey)))
        {
            goto Cleanup;
        }
    }

    // Output results
	if(logging) {
		if(FAILED(hr = supporter.displayKey(keyName, pbPubKey, cbPubKey, 0)))
		{
			goto Cleanup;
		}
	}

Cleanup:
    if(hKey != NULL)
    {
        NCryptFreeObject(hKey);
        hKey = NULL;
    }
    if(hProv != NULL)
    {
        NCryptFreeObject(hProv);
        hProv = NULL;
    }
    supporter.callResult(L"getPubKey()", hr);
    return hr;
}

HRESULT TPM_Utils::registerAIK()
	/*++
This function will register an AIK in the registry. Every time the machine
boots or resumes from hibernation, it will generate a Quote in the log and make
it permanently trustworthy. There may be multiple keys registered at the same
time and the system will make a Quote with each key. However this may lead
system boot time degradation. An average 1.2 TPM for example requires around
500ms to create a quote in the log.
--*/
{
    HRESULT hr = S_OK;
    NCRYPT_PROV_HANDLE hProv = NULL;
    NCRYPT_KEY_HANDLE hKey = NULL;
    PCWSTR keyName = NULL;
    PBYTE pbAik = NULL;
    UINT32 cbAik = 0;
    HKEY hRegKey = NULL;


    // Mandatory Parameter: AIK name
	keyName = keyAIKGlobal.c_str();

    // Export AIK pub
    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenStorageProvider(
                                &hProv,
                                MS_PLATFORM_CRYPTO_PROVIDER,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenKey(
                                hProv,
                                &hKey,
                                keyName,
                                0,
                                0))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptExportKey(
                                hKey,
                                NULL,
                                BCRYPT_OPAQUE_KEY_BLOB,
                                NULL,
                                NULL,
                                0,
                                (PDWORD)&cbAik,
                                0))))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbAik, cbAik)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = HRESULT_FROM_WIN32(NCryptExportKey(
                                hKey,
                                NULL,
                                BCRYPT_OPAQUE_KEY_BLOB,
                                NULL,
                                pbAik,
                                cbAik,
                                (PDWORD)&cbAik,
                                0))))
    {
        goto Cleanup;
    }

    // Register AIK for trust point generation
    if(FAILED(hr = HRESULT_FROM_WIN32(RegCreateKeyW(
                                HKEY_LOCAL_MACHINE,
                                TPM_STATIC_CONFIG_QUOTE_KEYS,
                                &hRegKey))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(RegSetValueExW(
                                hRegKey,
                                keyName,
                                NULL,
                                REG_BINARY,
                                pbAik,
                                cbAik))))
    {
        goto Cleanup;
    }

    RegCloseKey(hRegKey);
    hRegKey = NULL;

    // Register AIK for key attestation generation
    if(FAILED(hr = HRESULT_FROM_WIN32(RegCreateKeyW(
                                HKEY_LOCAL_MACHINE,
                                TPM_STATIC_CONFIG_KEYATTEST_KEYS,
                                &hRegKey))))
    {
        goto Cleanup;
    }

    if(FAILED(hr = HRESULT_FROM_WIN32(RegSetValueExW(
                                hRegKey,
                                keyName,
                                NULL,
                                REG_BINARY,
                                pbAik,
                                cbAik))))
    {
        goto Cleanup;
    }

    // Output results
	if(logging) {
		wprintf(L"Key '%s' registered. OK!\n", keyName);
	}
Cleanup:
    if(hRegKey != NULL)
    {
        RegCloseKey(hRegKey);
        hRegKey = NULL;
    }
    if(hKey != NULL)
    {
        NCryptFreeObject(hKey);
        hKey = NULL;
    }
    if(hProv != NULL)
    {
        NCryptFreeObject(hProv);
        hProv = NULL;
    }
    ZeroAndFree((PVOID*)&pbAik, cbAik);
    supporter.callResult(L"registerAIK()", hr);
    return hr;
}

HRESULT TPM_Utils::getPlatformAttestation(bool generateXml) 
/*++
This function will create an AIK signed platform attestation blob. In this case
the attestation is signed and nonces are supported. This attestation is tamper
proof.
--*/
{
    HRESULT hr = S_OK;
    NCRYPT_PROV_HANDLE hProv = NULL;
    NCRYPT_KEY_HANDLE hAik = NULL;
    PCWSTR aikName = NULL;
    PCWSTR attestationFile = NULL;
    PCWSTR nonce = NULL;
    PCWSTR aikAuthValue = NULL;
    PBYTE pbAttestation = NULL;
    UINT32 cbAttestation = 0;
    BYTE nonceDigest[20] = {0};
    UINT32 result = 0;
	FILE *stream;

	if(generateXml) {
		HANDLE hFile;
		// Convert string to LPCWSTR for filecreation
		std::wstring stemp = std::wstring(exportedPlatformAttAsXml.begin(), exportedPlatformAttAsXml.end());
		LPCWSTR sw = stemp.c_str();
		hFile = CreateFileW(sw,			// name of the write
				GENERIC_WRITE,			// open for writing
				0,						// do not share
				NULL,					// default security
				OPEN_EXISTING,			// Open existing file
				FILE_ATTRIBUTE_NORMAL,	// normal file
				NULL);					// no attr. template


		if((stream = freopen(exportedPlatformAttAsXml.c_str(), "w+", stdout)) == NULL) {
			printf("Error on freopen: can�t change stdout (console) to output.xml");
			return false;
		}
	}


    // Mandatory parameter: Aik name
	aikName = keyAIKGlobal.c_str();
   
    // Optional parameter: Attestation file
	attestationFile = attestationBlobGlobal.c_str();

    // Optional parameter: Nonce
	nonce = attestationNonceGlobal.c_str();
	if(FAILED(hr = TpmAttiShaHash(
	                        BCRYPT_SHA1_ALGORITHM,
	                        NULL,
	                        0,
	                        (PBYTE)nonce,
	                        (UINT32)(wcslen(nonce) * sizeof(WCHAR)),
	                        nonceDigest,
	                        sizeof(nonceDigest),
	                        &result)))
	{
	    goto Cleanup;
	}

    if(wcslen(aikName) > 0)
    {
        // Open AIK
        if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenStorageProvider(
                                    &hProv,
                                    MS_PLATFORM_CRYPTO_PROVIDER,
                                    0))))
        {
            goto Cleanup;
        }
        if(FAILED(hr = HRESULT_FROM_WIN32(NCryptOpenKey(
                                    hProv,
                                    &hAik,
                                    aikName,
                                    0,
                                    0))))
        {
            goto Cleanup;
        }
        if((aikAuthValue != NULL) && (wcslen(aikAuthValue) != 0))
        {
            if(FAILED(hr = HRESULT_FROM_WIN32(NCryptSetProperty(
                                        hAik,
                                        NCRYPT_PIN_PROPERTY,
                                        (PBYTE)aikAuthValue,
                                        (DWORD)((wcslen(aikAuthValue) + 1) * sizeof(WCHAR)),
                                        0))))
            {
                goto Cleanup;
            }
        }
    }

    if(FAILED(hr = TpmAttGeneratePlatformAttestation(
                                hAik,
                                0x0000f77f, // Default PCR mask PCR[0-6, 8-10, 12-15]
                                (nonce) ? nonceDigest : NULL,
                                (nonce) ? sizeof(nonceDigest) : 0,
                                NULL,
                                0,
                                &cbAttestation)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbAttestation, cbAttestation)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = TpmAttGeneratePlatformAttestation(
                                hAik,
                                0x0000f77f, // Default PCR mask PCR[0-6, 8-10, 12-15]
                                (nonce) ? nonceDigest : NULL,
                                (nonce) ? sizeof(nonceDigest) : 0,
                                pbAttestation,
                                cbAttestation,
                                &cbAttestation)))
    {
        goto Cleanup;
    }

    // Export attestation blob
	if(attestationFile != NULL)
    {
        if(FAILED(hr = supporter.writeFile(
                                attestationFile,
                                pbAttestation,
                                cbAttestation)))
        {
            goto Cleanup;
        }
    }

    // Output results
	if(logging || generateXml) {
		printf("");
		wprintf(L"<PlatformAttestation size=\"%u\">\n", cbAttestation);
		if(FAILED(hr = supporter.displayPlatformAttestation(pbAttestation, cbAttestation, 1)))
		{
			goto Cleanup;
		}
		wprintf(L"</PlatformAttestation>\n");
	}

	if(generateXml) {
		stream = freopen("CON", "w", stdout);
	}


Cleanup:
    if(hAik != NULL)
    {
        NCryptFreeObject(hAik);
        hAik = NULL;
    }
    if(hProv != NULL)
    {
        NCryptFreeObject(hProv);
        hProv = NULL;
    }
    ZeroAndFree((PVOID*)&pbAttestation, cbAttestation);
    supporter.callResult(L"getPlatformAttestation()", hr);
    return hr;
}

HRESULT TPM_Utils::validatePlatformAttestation(std::wstring _attestation, std::wstring _aikPub)
/*++
This function will validate an AIK signed platform attestation blob. In this case
the attestation is signed and nonces are supported. This attestation is tamper
proof.
--*/
{
	
    HRESULT hr = S_OK;
    PCWSTR aikName = NULL;
    PCWSTR attestationFile = NULL;
    PCWSTR nonce = NULL;
    BCRYPT_ALG_HANDLE hAlg = NULL;
    BCRYPT_KEY_HANDLE hAik = NULL;
    PBYTE pbAttestation = NULL;
    UINT32 cbAttestation = 0;
    PBYTE pbAikPub = NULL;
    UINT32 cbAikPub = 0;
    BYTE nonceDigest[20] = {0};
    UINT32 cbNonceDigest = sizeof(nonceDigest);
	
    // Mandatory paremeter: Attestation blob file
	attestationFile = _attestation.c_str();
    if(FAILED(hr = supporter.readFile(
                            attestationFile,
                            NULL,
                            0,
                            &cbAttestation)))
    {
        goto Cleanup;
    }

    if(FAILED(hr = AllocateAndZero((PVOID*)&pbAttestation, cbAttestation)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = supporter.readFile(
                            attestationFile,
                            pbAttestation,
                            cbAttestation,
                            &cbAttestation)))
    {
        goto Cleanup;
    }

    // Mandatory paremeter: AIK pub
	aikName = _aikPub.c_str(); 
	if(wcslen(aikName) > 0)
    {

        if(FAILED(hr = supporter.readFile(aikName, NULL, 0, &cbAikPub)))
        {
            goto Cleanup;
        }
        if(FAILED(hr = AllocateAndZero((PVOID*)&pbAikPub, cbAikPub)))
        {
            goto Cleanup;
        }

        if(FAILED(hr = supporter.readFile(
                                aikName,
                                pbAikPub,
                                cbAikPub,
                                &cbAikPub)))
        {
            goto Cleanup;
        }
    }
	
    // Optional paremeter: Nonce
	nonce = attestationNonceGlobal.c_str();
    if(wcslen(nonce) > 0)
    {
        if(FAILED(hr = TpmAttiShaHash(
                                BCRYPT_SHA1_ALGORITHM,
                                NULL,
                                0,
                                (PBYTE)nonce,
                                (UINT32)(wcslen(nonce) * sizeof(WCHAR)),
                                nonceDigest,
                                sizeof(nonceDigest),
                                &cbNonceDigest)))
								// Must sleep until tpm/file system has finished his work
								Sleep(3000);
        {
            goto Cleanup;
        }
    }
    else
    {
        nonce = NULL;
    }
	
    // Load the AIKPub
    if(wcslen(aikName) > 0)
    {
        if(FAILED(hr = HRESULT_FROM_NT(BCryptOpenAlgorithmProvider(
                                        &hAlg,
                                        BCRYPT_RSA_ALGORITHM,
                                        MS_PRIMITIVE_PROVIDER,
                                        0))))
        {
            goto Cleanup;
        }
        if(FAILED(hr = HRESULT_FROM_NT(BCryptImportKeyPair(
                                        hAlg,
                                        NULL,
                                        BCRYPT_RSAPUBLIC_BLOB,
                                        &hAik,
                                        pbAikPub,
                                        cbAikPub,
                                        0))))
        {
            goto Cleanup;
        }
    }

    // Validate the Attestation Blob
    if(FAILED(hr = TpmAttValidatePlatformAttestation(
                                   hAik,
                                   (nonce) ? nonceDigest : NULL,
                                   (nonce) ? sizeof(nonceDigest) : 0,
                                   pbAttestation,
                                   cbAttestation)))
    {
        goto Cleanup;
    }


    // Output
	if(logging) {
		wprintf(L"Verified - OK.\n");
	}
Cleanup:
    if(hAik != NULL)
    {
        BCryptDestroyKey(hAik);
        hAik = NULL;
    }
    if(hAlg != NULL)
    {
        BCryptCloseAlgorithmProvider(hAlg, 0);
        hAlg = NULL;
    }
    ZeroAndFree((PVOID*)&pbAttestation, cbAttestation);
    ZeroAndFree((PVOID*)&pbAikPub, cbAikPub);
    supporter.callResult(L"validatePlatformAttestation()", hr);
    return hr;
}


HRESULT TPM_Utils::getLog()
/*++
Obtain the current Windows Boot Configuration Log (WBCL) for the platform. This
log can be used to calculate the PCRs in the TPM.
--*/
{
    HRESULT hr = S_OK;
    PCWSTR fileName = NULL;
    TBS_HCONTEXT hContext = NULL;
    TBS_CONTEXT_PARAMS2 contextParams = {0};
    TPM_DEVICE_INFO deviceInfo = {0};
    PBYTE pbLog = NULL;
    UINT32 cbLog = 0;
    PWSTR TpmVersion[] = {L"TPM_VERSION_UNKNOWN",
                          L"TPM_VERSION_12",
                          L"TPM_VERSION_20"};
    const UINT32 TpmVersionCount = 3;
    PWSTR TpmIFType[] = {L"TPM_IFTYPE_UNKNOWN",
                         L"TPM_IFTYPE_1",
                         L"TPM_IFTYPE_TRUSTZONE",
                         L"TPM_IFTYPE_HW",
                         L"TPM_IFTYPE_EMULATOR"};
    const UINT32 TpmIFTypeCount = 5;

    // Optional parameter: Export file for log

	fileName = exportedLogFile.c_str();

    // Open TBS and read the current log
    contextParams.version = TBS_CONTEXT_VERSION_TWO;
    contextParams.asUINT32 = 0;
    contextParams.includeTpm12 = 1;
    contextParams.includeTpm20 = 1;
    if(FAILED(hr = Tbsi_Context_Create((PTBS_CONTEXT_PARAMS)&contextParams, &hContext)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = Tbsi_GetDeviceInfo(sizeof(deviceInfo), &deviceInfo)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = Tbsi_Get_TCG_Log(hContext, NULL, &cbLog)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbLog, cbLog)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = Tbsi_Get_TCG_Log(hContext, pbLog, &cbLog)))
    {
        goto Cleanup;
    }

    // Write log to file if requested
    if(fileName != NULL)
    {
       if(FAILED(hr = supporter.writeFile(fileName, pbLog, cbLog)))
        {
            goto Cleanup;
        }
    }

	if(logging) {
		wprintf(L"<PlatformInfo>\n");
		if(deviceInfo.structVersion == 1)
		{
			supporter.levelPrefix(1);
			wprintf(L"<DeviceInfo>\n");
			supporter.levelPrefix(2);
			wprintf(L"<TPMVersion>%s</TPMVersion>\n", deviceInfo.tpmVersion < TpmVersionCount ? TpmVersion[deviceInfo.tpmVersion] : TpmVersion[0]);
			supporter.levelPrefix(2);
			wprintf(L"<TPMInterfaceType>%s</TPMInterfaceType>\n", deviceInfo.tpmInterfaceType < TpmIFTypeCount ? TpmIFType[deviceInfo.tpmInterfaceType] : TpmIFType[0]);
			supporter.levelPrefix(2);
			wprintf(L"<TPMImplementationRevision>%d</TPMImplementationRevision>\n", deviceInfo.tpmImpRevision);
			supporter.levelPrefix(1);
			wprintf(L"</DeviceInfo>\n");
		}
		else
		{
			supporter.levelPrefix(1);
			wprintf(L"<DeviceInfo>INVALID</DeviceInfo>\n");
		}
	
		// Output result
		if(FAILED(hr = supporter.displayLog(pbLog, cbLog, 1)))
		{
		 goto Cleanup;
		}
		wprintf(L"</PlatformInfo>\n");
	}
Cleanup:
    if (hContext != NULL)
    {
        Tbsip_Context_Close(hContext);
        hContext = NULL;
    }
    ZeroAndFree((PVOID*)&pbLog, cbLog);
    supporter.callResult(L"getLog()", hr);
    return hr;
}

HRESULT TPM_Utils::createPlatformAttestationFromLog(std::wstring _logFile, std::wstring _attestationFile)
	{
    HRESULT hr = S_OK;
    PCWSTR logFile = NULL;
    PCWSTR fileName = NULL;
    PCWSTR requestedAikName = NULL;
    PWSTR aikName = NULL;
    PBYTE pbLog = NULL;
    UINT32 cbLog = 0;
    PBYTE pbAttestation = NULL;
    UINT32 cbAttestation = 0;


    // Mandatory paremeter: Log file
	logFile = _logFile.c_str();
    if(FAILED(hr = supporter.readFile(
                            logFile,
                            NULL,
                            0,
                            &cbLog)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbLog, cbLog)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = supporter.readFile(
                            logFile,
                            pbLog,
                            cbLog,
                            &cbLog)))
    {
        goto Cleanup;
    }
    
    // Optional parameter: Export file for log
	fileName = _attestationFile.c_str();

	// Optional parameter: Requested AIK name
	requestedAikName = keyAIKGlobal.c_str(); // If no key is given, the lib uses "Windows AIK" as name
	 
    // Turn the log with trustpoints into an attestation blob
    if(FAILED(hr = TpmAttCreateAttestationfromLog(pbLog,
                                                  cbLog,
                                                  (PWSTR)requestedAikName,
                                                  &aikName,
                                                  NULL,
                                                  NULL,
                                                  0,
                                                  &cbAttestation)))
    {
        goto Cleanup;
    }
	
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbAttestation, cbAttestation)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = TpmAttCreateAttestationfromLog(pbLog,
                                                  cbLog,
                                                  (PWSTR)requestedAikName,
                                                  &aikName,
                                                  NULL,
                                                  pbAttestation,
                                                  cbAttestation,
                                                  &cbAttestation)))
    {
        goto Cleanup;
    }

    // Write log to file if requested
    if(fileName != NULL)
    {
       if(FAILED(hr = supporter.writeFile(fileName, pbAttestation, cbAttestation)))
        {
            goto Cleanup;
        }
    }

    // Output results
	if(logging) {
		wprintf(L"AIK identifier for Trustpoint: '%s'. Log converted - OK!\n", aikName);
	}
Cleanup:
    ZeroAndFree((PVOID*)&pbLog, cbLog);
    ZeroAndFree((PVOID*)&pbAttestation, cbAttestation);
    supporter.callResult(L"createPlatformAttestationFromLog()", hr);
    return hr;
}

HRESULT TPM_Utils::getArchivedLog()
{
    HRESULT hr = S_OK;
    UINT32 bootCount = 0;
    UINT32 resumeCount = 0;
    PCWSTR fileName = NULL;
    PBYTE pbLog = NULL;
    UINT32 cbLog = 0;


    // Mandatory parameter: OSBootCount

	if(!_wcsicmp(oSBootCountParam.c_str(), L"@"))
    {
        if(FAILED(hr = TpmAttGetPlatformCounters(
                            &bootCount,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL
                            )))
        {
            goto Cleanup;
        }
    }
    else
    {
        if(swscanf_s(oSBootCountParam.c_str(), L"%d", &bootCount) != 1)
        {
            hr = E_INVALIDARG;
            goto Cleanup;
        }
    }
    

    // Mandatory parameter: OSResumeCount

	if(!_wcsicmp(oSResumeCountParam.c_str(), L"@"))
        {
            if(FAILED(hr = TpmAttGetPlatformCounters(
                                NULL,
                                &resumeCount,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL
                                )))
            {
                goto Cleanup;
            }
        }
        else
        {
            if(swscanf_s(oSResumeCountParam.c_str(), L"%d", &resumeCount) != 1)
            {
                hr = E_INVALIDARG;
                goto Cleanup;
            }
        }

    // Optional parameter: Export file for log

	fileName = lastBootLogFile.c_str();

    if(FAILED(hr = TpmAttGetPlatformLogFromArchive(
                        bootCount,
                        resumeCount,
                        NULL,
                        0,
                        &cbLog)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = AllocateAndZero((PVOID*)&pbLog, cbLog)))
    {
        goto Cleanup;
    }
    if(FAILED(hr = TpmAttGetPlatformLogFromArchive(
                        bootCount,
                        resumeCount,
                        pbLog,
                        cbLog,
                        &cbLog)))
    {
        goto Cleanup;
    }

    // Write log to file if requested
    if(fileName != NULL)
    {
       if(FAILED(hr = supporter.writeFile(fileName, pbLog, cbLog)))
        {
            goto Cleanup;
        }
    }

    // Show the log
	if(logging)
	{
		if(FAILED(hr = supporter.displayLog(pbLog, cbLog, 0)))
		{
			goto Cleanup;
		}
	}

Cleanup:
    ZeroAndFree((PVOID*)&pbLog, cbLog);
    supporter.callResult(L"getArchivedLog()", hr);
    return hr;
}


bool TPM_Utils::runMeasuredBoot() {

	std::string loadedModuleFilePath = "";
	std::string elamDriverPath = "";
	std::map<std::string, bool> bootBins;
	std::map<std::string,bool>::iterator it;
	std::string elam = "";
	std::string sign = "";

	// run PlatformAttestation again, but this time store the output as xml
	if(FAILED(getPlatformAttestation(true)))	{
		return false;
	}

	TiXmlDocument doc(exportedPlatformAttAsXml.c_str());
	bool loadOkay = doc.LoadFile();
	TiXmlElement* root = doc.FirstChildElement();
	TiXmlElement* child = root->FirstChild( "Log" )->FirstChild( "TCGLog" )->FirstChild( "WBCL" )->ToElement();



	if (!loadOkay) {
		printf("Failed to load file \"%s\"\n", exportedPlatformAttAsXml.c_str());
		return false;
	}

	for(TiXmlElement* childEV = child->FirstChildElement("EV_Event_Tag"); childEV != NULL; childEV = childEV->NextSiblingElement("EV_Event_Tag")) {
		if (NULL == childEV->FirstChild("Trustboundary")) {
			continue;
		}

		TiXmlElement* childTrust = childEV->FirstChild("Trustboundary")->ToElement();

		for(TiXmlElement* childModule = childTrust->FirstChildElement("LoadedModule_Aggregation"); childModule != NULL; childModule = childModule->NextSiblingElement("LoadedModule_Aggregation")) {
			if (NULL == childModule->FirstChildElement("FilePath")) {
				continue;
			}
			loadedModuleFilePath = childModule->FirstChild("FilePath")->ToElement()->GetText();
			if (NULL != childModule->FirstChild("ImageValidated")) {
				std::string image = childModule->FirstChild("ImageValidated")->ToElement()->GetText();
				if(image == "TRUE") {
					bootBins.insert(std::pair<std::string,bool>(loadedModuleFilePath,true));
				}
				else {
					bootBins.insert(std::pair<std::string,bool>(loadedModuleFilePath,false));
				}
			}
			else if (0 == bootBins.count(loadedModuleFilePath)) {
				bootBins.insert(std::pair<std::string,bool>(loadedModuleFilePath,false));
			}
			TiXmlElement * authorityHashEle = childModule->FirstChild("AuthoritySHA1Thumbprint")->ToElement();
			if (NULL == authorityHashEle) {
				continue;
			}
			if(authorityHashEle->GetText() == "c781d24b3d08cfab8a61b960e77c0cd6316e3d56") {
				elamDriverPath = loadedModuleFilePath;
			}



		}

	}


	bool signedBootBinaries = true;
	
	printf("Early boot binaries:\n");
	for (it=bootBins.begin(); it!=bootBins.end(); ++it) {

		if(it->first == elamDriverPath) {
			elam = " (ELAM)";
		}
		else {
			elam = "";
		}
		if(it->second) {
			sign = "SIGNED";
		}
		else {
			signedBootBinaries = false;
			sign = "UNSIGNED!";
		}

		printf(it->first.c_str());
		printf(" ");
		printf(elam.c_str());
		printf(" -- ");
		printf(sign.c_str());
		printf("\n");

	}

	return signedBootBinaries;
}

