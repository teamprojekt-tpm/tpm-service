// TPM_Full_Attestation.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//
#include "stdafx.h"
#include "TPM_Utils.h"
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{

	// Load Tpm Attestation functionality
	TPM_Utils utils;
	bool ok = utils.runAttestation();
	
	return ok;

}

