﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.IO;
using System.IO.Pipes;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TrustedConsole
{

    [ServiceContract]
    public interface IPipeInterfaceTrustedPc
    {
        [OperationContract]
        String getSecret();
    }

    /// example app which reveals secret form card if PC is trusted
    public class Program
    {

        /// start the client pipe and prints secret to console
        public void startClientPipe()
        {

            ChannelFactory<IPipeInterfaceTrustedPc> pipeFactory =
              new ChannelFactory<IPipeInterfaceTrustedPc>(
                new NetNamedPipeBinding(),
                new EndpointAddress(
                 "net.pipe://localhost/revealSecret"));

            IPipeInterfaceTrustedPc pipeProxy = pipeFactory.CreateChannel();

            try
            {
                Console.WriteLine("You did it! The secret is:\n" + pipeProxy.getSecret());
                Thread.Sleep(10000);

            }
            catch (Exception e)
            {
                Console.WriteLine("EXC:\n" + e.Message);
                Thread.Sleep(10000);
            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.startClientPipe();
        }

        /// get current installation path
        public static String getCurrentPath()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().Location;
        }
    }
}
