﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.IO;
using System.IO.Pipes;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;


namespace ConsoleCheckPin
{

    [ServiceContract]
    public interface IPipeInterface
    {
        [OperationContract]
        void setPin(String pin);
    }

    /// <summary>
    /// application used to get PIN from user
    /// </summary>
    public class Program
    {

        /// <summary>
        /// start the client pipe and read PIN from console
        /// </summary>
        public void startClientPipe()
        {

            ChannelFactory<IPipeInterface> pipeFactory =
              new ChannelFactory<IPipeInterface>(
                new NetNamedPipeBinding(),
                new EndpointAddress(
                 "net.pipe://localhost/setPin"));

            IPipeInterface pipeProxy = pipeFactory.CreateChannel();

            try
            {
                Console.WriteLine("Enter PIN");
                String pin = Console.ReadLine();
                pipeProxy.setPin(pin);
                Console.WriteLine("checking entered PIN");
                Thread.Sleep(1000);

            }
            catch (Exception e)
            {
                Console.WriteLine("EXC:\n" + e.Message);
                Thread.Sleep(5000);
            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.startClientPipe();
        }

        /// <summary>
        /// get current installation path
        /// </summary>
        /// <returns></returns>
        public static String getCurrentPath()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().Location;
        }
    }
}
