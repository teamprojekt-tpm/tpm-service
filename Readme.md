# Teamprojekt Trusted Path with TPM - TPM-Service Module

This module is a core component to communicate with the TPM itself.

## Getting started

### Clone the repository
```
cd your/dream/directory
git clone https://git@bitbucket.org/teamprojekt-tpm/tpm-service.git tpm-service
```

### Preconditions
* activated TPM v1.2 or 2.0
* .NET Framework 4.0+
* admin rights ;-)
* running smard card application

### Development Tools
* Microsoft Visual Studio
* InstallShield AddIn
* BasicCard Framework 3.5

## Working with git

Do NOT work on master branch!

For some great git commands, see: http://overapi.com/git/

Ideally create feature branches, e.g.:
```
git checkout -b feature-super-great-feature
```

### Install Windows Service

#### Using the InstallShield Installer

In Visual Studio just right click on the 'ServiceInstaller' and choose 'Install'. Click through the installation dialogue and you'll be done. With this steps an eventually already installed service will get uninstalled, newly installed and started.
If you just want to uninstall it, choose 'Uninstall' instead. Alternatively use 'Windows Programs and Features'.
Also an executable installer is generated, so it should be possible to port it to another machine and use it there.

#### Using CommandLine

Um den Beispiel-Windows-Dienst zu installieren benötigt man das .Net Framework und die CMD (als Admin ausführen):
Beispiel Installieren:
```
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installutil.exe C:\Users\Fabian\Desktop\Debug\WindowsServiceSmartCard.exe
```
Wobei der erste Pfad der zum Framework und dem "installutil.exe" ist (wird benötigt um Dienste zu installieren) und der zweite Pfad stellt den kompillierten Dienst dar (hier im "Debug"-Ordner um die Debug-Meldungen zu erhalten)

Deinstallieren:
```
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installutil.exe /u C:\Users\Fabian\Desktop\Debug\WindowsServiceSmartCard.exe
```
Hier nur das "/u" beachten, sonst gleich wie oben beim Installieren

Zum Prüfen kann man in Windows unter "lokale Dienste anzeigen" gehen. Die Debugmeldungen kann man unter Ereignisprotokolle anzeigen" einsehen (in dem Beispiel unter "Anwendungs- und Dienstprotokolle"->"MyNewLog")