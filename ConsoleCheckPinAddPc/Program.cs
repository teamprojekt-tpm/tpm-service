﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.IO;
using System.IO.Pipes;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleCheckPinAddPc
{
    [ServiceContract]
    public interface IPipeInterfaceAddingPc
    {
        [OperationContract]
        void setPinForAddingPc(String pin);
    }

    public class Program
    {

        public void startClientPipe()
        {

            ChannelFactory<IPipeInterfaceAddingPc> pipeFactory =
              new ChannelFactory<IPipeInterfaceAddingPc>(
                new NetNamedPipeBinding(),
                new EndpointAddress(
                 "net.pipe://localhost/setPinForAddingPc"));

            IPipeInterfaceAddingPc pipeProxy = pipeFactory.CreateChannel();

            try
            {
                Console.WriteLine("Enter PIN for adding new pc:");
                String pin = Console.ReadLine();
                pipeProxy.setPinForAddingPc(pin);

                Console.WriteLine("checking entered PIN");
                Thread.Sleep(1000);

            }
            catch (Exception e)
            {
                Console.WriteLine("EXC:\n" + e.Message);
                Thread.Sleep(3000);
            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.startClientPipe();
        }

        public static String getCurrentPath()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().Location;
        }
    }
}
